import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });


  // todo: show fighter info (image, name, health, etc.)

  function fighterPreviewInfo(fighterKey) {
    const newElement = createElement({tagName: 'span', className: 'fighter-preview___property'});
    const keyInfo = fighterKey[0].slice(0, 1).toUpperCase() + fighterKey[0].slice(1);
    const valueInfo = fighterKey[1];
    newElement.innerText = keyInfo + ' : ' + valueInfo;
    newElement.style.cssText = 'color: #fff';
    return newElement;
  }

  function fighterPreviewImage(sourceImg) {
    const attributes = {src: sourceImg};
    const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
    });

    if(position === 'right') {
      imgElement.style.transform = 'scale(-1, 1)';
    }

    return imgElement;
  }

  if (fighter) {
      fighterElement.append(fighterPreviewImage(fighter.source));
      let infoAboutFighters = Object.entries(fighter);
      infoAboutFighters = infoAboutFighters.filter(fighterInfo => fighterInfo[0] !== '_id' && fighterInfo[0] !== 'source');
      infoAboutFighters.forEach(fighterInfo => fighterElement.append(fighterPreviewInfo(fighterInfo)));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
