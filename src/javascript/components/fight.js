import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    const healthBarsContainer = document.getElementsByClassName('arena___health-bar');
    const healthBars = [ ...healthBarsContainer ];

    const firstPlayer = {
      ...firstFighter,
      healthBar: healthBars[0],
      block: false,
      currentHealth: 100,
      position: 'left'
    }

    const playerTwo = {
      ...secondFighter,
      healthBar: healthBars[1],
      block: false,
      currentHealth: 100,
      position: 'right'
    }

    function attackRelease(attacker, defender) {

      const totalDamage = getDamage(attacker, defender);

      defender.currentHealth = defender.currentHealth - totalDamage / defender.health * 100;
      if(defender.currentHealth <= 0) {
        document.removeEventListener('keydown', onKeyDown);
        resolve(attacker);
      }

      defender.healthBar.style.width = `${defender.currentHealth}%`;
    }

    function onKeyDown(event) {
      if(!event.repeat) {
        switch(event.code) {
          case controls.PlayerOneAttack: {
            attackRelease(firstPlayer, playerTwo);
            break;
          }

          case controls.PlayerTwoAttack: {
            attackRelease(playerTwo, firstPlayer);
            break;
          }

          case controls.PlayerOneBlock: {
            firstPlayer.block = true;
            break;
          }

          case controls.PlayerTwoBlock: {
            playerTwo.block = true;
            break;
          }
        }
      }
    }

    document.addEventListener('keydown', onKeyDown);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = fighter.critInput === 3 ? 2 : Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
